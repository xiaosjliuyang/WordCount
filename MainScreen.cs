﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyWordCount
{
    class MainScreen//主操作类
    {
        public void mainScreen()//构造函数
        {
            while (true)
            {
                Console.WriteLine("****************************************");
                Console.WriteLine("*************WordCount统计**************");
                Console.WriteLine("****************************************");
                Console.WriteLine("统计字符数        wc.exe -c file.c  ");
                Console.WriteLine("统计单词总数      wc.exe -w file.c  ");
                Console.WriteLine("统计总行数        wc.exe -l file.c  ");
                Console.WriteLine("统计输出到文件    wc.exe -o outputfile.txt  ");
                Console.WriteLine("****************************************");
                Console.WriteLine("请输入指令：");
                string cmd = Console.ReadLine();//输入的操作指令
                Count wc = new Count();
                string[] array = cmd.Split(new char[2] { ' ', '-' }, StringSplitOptions.RemoveEmptyEntries);//去空格截取输入的字符串信息
                for (int i = 1; i < array.Length - 1; i++)
                {
                    switch (array[i])
                    {
                        case "c":
                            Console.WriteLine(array[array.Length - 1] + "  字符数：" + wc.countC(array[array.Length - 1]));
                            break;
                        case "w":
                            Console.WriteLine(array[array.Length - 1] + "  单词数：" + wc.countW(array[array.Length - 1]));
                            break;
                        case "l":
                            Console.WriteLine(array[array.Length - 1] + "  行数：" + wc.countL(array[array.Length - 1]));
                            break;
                        case "o":
                            Console.WriteLine(wc.countO(array[array.Length - 1]));
                            break;
                        default:
                            Console.WriteLine("输入的指令 -" + array[i] + " 有误！");
                            break;
                    }
                }
                Console.ReadKey();//等待用户按下任意键，一次读入一个字符。
            }

        }
    }
}
