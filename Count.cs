﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace MyWordCount
{
    /// <summary>
    /// 指令操作
    /// </summary>
    class Count//用于具体计数操作的类
    {
        /// <summary>
        /// 统计文件中的字符个数
        /// </summary>
        /// <param name="fstr">要统计的文件名</param>
        /// <returns>返回字符数</returns>
        public int countC(string fs)//统计字符数
        {
            string str = openFile(fs);
            int Count = 2;//记录字符个数
            foreach (char c in str)
            {
                if (c ==' ' ||c=='\t'|| c == '\n')
                {
                    Count++;
                }
            }
            int count=str.Length-Count;
            StreamWriter sw = new StreamWriter("result.txt", true);
            sw.WriteLine(fs + "  字符数： " + Convert.ToString(count));
            sw.Close();
            return count;
        }

        /// <summary>
        /// 统计文件中的单词数
        /// </summary>
        /// <param name="wstr">要统计的文件名</param>
        /// <returns>返回单词数</returns>
        public int countW(string wstr)
        {
            string str = openFile(wstr);
            int count = 0;//记录单词个数
            bool isblank = true;
            foreach (char s in str)//遍历字符串，
            {
                if (s != ' ' && s != '\n' && isblank == true)
                {
                    count++;
                    isblank = false;
                }
                else if ((s == ' ' || s == '\n') && isblank == false)
                {
                    isblank = true;
                }
            }
            StreamWriter sw = new StreamWriter("result.txt", true);
            sw.WriteLine(wstr + "  单词数：" + Convert.ToString(count - 1));//
            sw.Close();
            return count - 1;
        }

        /// <summary>
        /// 统计文件中的行数
        /// </summary>
        /// <param name="lstr">要统计的文件名</param>
        /// <returns>返回行数</returns>
        public int countL(string lstr)
        {
            string str = openFile(lstr);
            int count = 1;//记录行数
            foreach (char s in str)
            {
                if (s == '\n')
                {
                    count++;
                }
            }
            StreamWriter sw = new StreamWriter("result.txt", true);
            sw.WriteLine(lstr + "   行数： " + Convert.ToString(count - 1));
            sw.Close();
            return count;
        }

         /// <summary>
        /// 输出结果到指定文件
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool countO(string oFile)
        {
            string str = openFile("result.txt");//打开结果保存文件

            StreamWriter sw = new StreamWriter(oFile, true);
            sw.WriteLine(str);
            sw.Close();
            return true;
            
        }

        /// <summary>
        /// 打开文件
        /// </summary>
        /// <param name="fstr">文件名</param>
        /// <returns>返回字符串</returns>
        public string openFile(string fstr)
        {

            FileStream fs = new FileStream(@fstr, FileMode.Open);//初始化文件流
            byte[] array = new byte[fs.Length];//初始化字节数组
            fs.Read(array, 0, array.Length);//读取流中数据到字节数组中
            fs.Close();//关闭文件
            string str = Encoding.UTF8.GetString(array);//解码文件，避免出现乱码现象
            return str;

        }
    }
}
